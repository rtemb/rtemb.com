<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Migration creates tables for storing sections and posts
 */
class Version20180501200601 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE posts (
              id INT AUTO_INCREMENT NOT NULL, 
              section_id INT NOT NULL, 
              title VARCHAR(255) NOT NULL, 
              preview VARCHAR(3000) NOT NULL, 
              text LONGTEXT NOT NULL, 
              date_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, 
              date_published DATETIME DEFAULT NULL, 
              keywords VARCHAR(255) DEFAULT NULL, 
              status INT NOT NULL, 
              INDEX IDX_885DBAFAD823E37A (section_id), 
              PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sections (
              id INT AUTO_INCREMENT NOT NULL, 
              name VARCHAR(50) NOT NULL, 
              PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFAD823E37A FOREIGN KEY (section_id) REFERENCES sections (id)');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFAD823E37A');
        $this->addSql('DROP TABLE posts');
        $this->addSql('DROP TABLE sections');
    }
}
