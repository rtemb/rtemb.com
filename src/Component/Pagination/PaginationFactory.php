<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 13/05/18
 * Time: 17:33
 */

namespace App\Component\Pagination;

use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class PaginationFactory
 *
 * @package App\Component\Pagination
 */
class PaginationFactory
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * PaginationFactory constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param QueryBuilder $qb
     * @param Request      $request
     * @param string       $route
     * @param int          $itemsOnPage
     * @param array        $routeParams
     *
     * @return PaginatedCollection
     */
    public function createCollection(
        QueryBuilder $qb,
        Request $request,
        string $route,
        int $itemsOnPage,
        array $routeParams = array()
    ) {
        $page = $request->query->getInt('page', 1);

        $adapter = new DoctrineORMAdapter($qb->orderBy('post.id', 'DESC'));
        $pagerfanta = new Pagerfanta($adapter);

        $pagerfanta
            ->setMaxPerPage($itemsOnPage)
            ->setCurrentPage($page);

        $posts = $pagerfanta->getCurrentPageResults();
        $postCollection = new PostPagination($posts, $pagerfanta->getNbResults());

        $routeParams = array_merge($routeParams, $request->query->all());

        $createLinkUrl = function($targetPage) use ($route, $routeParams) {
            return $this->router->generate($route, array_merge(
                $routeParams,
                array('page' => $targetPage)
            ));
        };

        $postCollection
            ->addLink('self', $createLinkUrl($page))
            ->addLink('first', $createLinkUrl(1))
            ->addLink('last', $createLinkUrl($pagerfanta->getNbPages()));

        if ($pagerfanta->hasNextPage()) {
            $postCollection->addLink('next', $createLinkUrl($pagerfanta->getNextPage()));
        }
        if ($pagerfanta->hasPreviousPage()) {
            $postCollection->addLink('prev', $createLinkUrl($pagerfanta->getPreviousPage()));
        }

        $postCollection->setNumOfPages($pagerfanta->getNbPages());
        $postCollection->setSection($request->get('section'));

        return $postCollection;
    }
}