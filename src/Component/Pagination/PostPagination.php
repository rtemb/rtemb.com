<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 06/10/2018
 * Time: 21:12
 */

namespace App\Component\Pagination;

class PostPagination extends PaginatedCollection
{
    /** @var string */
    private $section;

    /**
     * PostPagination constructor.
     *
     * @param array|\Traversable $items
     * @param int $total
     */
    public function __construct($items, int $total)
    {
        parent::__construct($items, $total);
    }

    /**
     * @return string
     */
    public function getSection(): string
    {
        return $this->section;
    }

    /**
     * @param string $section
     *
     * @return PostPagination
     */
    public function setSection($section): self
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @return array
     */
    public function getLinksForPage(): array
    {
        $LinksForPage = [];

        for ($page = 1; $page <= $this->getNumOfPages(); $page++) {
            if ($this->section) {
                $LinksForPage[] = '/?section=' . $this->getSection() .  '&page=' . $page;
            } else {
                $LinksForPage[] = '/?page=' . $page;

            }
        }

        return $LinksForPage;
    }
}