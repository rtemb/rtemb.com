<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 13/05/18
 * Time: 16:16
 */

namespace App\Component\Pagination;

use Traversable;

/**
 * Class PaginatedCollection
 *
 * @package App\Component\Pagination
 */
class PaginatedCollection
{
    /** @var array|\Traversable */
    private $items;

    /** @var int */
    private $total;

    /** @var int */
    private $count;

    /** @var int */
    private $numOfPages;

    /** @var array */
    private $links = [];

    /**
     * PaginatedCollection constructor.
     *
     * @param array|Traversable $items
     * @param int               $total
     */
    public function __construct($items, int $total)
    {
        $this->items = $items;
        $this->total = $total;
        $this->count = $items instanceof \Traversable ? \iterator_count($items) : count($items);
    }

    /**
     * @return array|\Traversable
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }


    /**
     * @param string $ref
     * @param string $url
     *
     * @return $this
     */
    public function addLink(string $ref, string $url): self
    {
        $this->links[$ref] = $url;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumOfPages(): int
    {
        return $this->numOfPages;
    }

    /**
     * @param int $numOfPages
     */
    public function setNumOfPages(int $numOfPages): void
    {
        $this->numOfPages = $numOfPages;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->links;
    }


    /**
     * @param array $links
     *
     * @return $this
     */
    public function setLinks(array $links): self
    {
        $this->links = $links;

        return $this;
    }

    /**
     * @return array
     */
    public function getLinksForPage(): array
    {
        $LinksForPage = [];

        for ($page = 1; $page <= $this->getNumOfPages(); $page++) {
            $LinksForPage[] = '/?page=' . $page;
        }

        return $LinksForPage;
    }

    /**
     * @return string
     */
    public function getPreviousPageLink()
    {
        return $this->getLinks()['prev'] ?? '';
    }

    /**
     * @return string
     */
    public function getNextPageLink()
    {
        return $this->getLinks()['next'] ?? '';
    }

    /**
     * @return string
     */
    public function getCurrentPageLink()
    {
        return $this->getLinks()['self'] ?? '';
    }
}