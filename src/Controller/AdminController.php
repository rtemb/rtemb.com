<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 10/02/18
 * Time: 00:44
 */

namespace App\Controller;

use App\Entity\Post;
use App\Form\Type\PostFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 *
 * @package App\Controller
 * @Route("/admin", name="admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/post/edit/{postId}", name="edit", defaults={"postId"=0})
     *
     * @param Request $request
     * @param int     $postId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postEditAction(Request $request, int $postId)
    {
        $post = null;
        if ($postId) {
            $post = $this->getDoctrine()->getRepository(Post::class)->find($postId);
        }

        $form = $this->createForm(PostFormType::class, $post)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
        }

        return $this->render('post-edit.html.twig', [
            'postForm' => $form->createView()
        ]);
    }
}