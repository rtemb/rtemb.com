<?php

namespace App\Controller;

use App\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class PostController
 *
 * @package App\Controller
 * @Route("/post")
 */
class PostController extends Controller
{
    /**
     * @Route("/read/{id}", name="post_read")
     * @Template("post/post.html.twig")
     * @param Post $post
     *
     * @return array
     */
    public function read(Post $post)
    {
        return ['post' => $post];
    }
}
