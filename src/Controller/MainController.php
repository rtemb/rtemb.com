<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 10/02/18
 * Time: 00:44
 */

namespace App\Controller;

use App\Component\Pagination\PaginationFactory;
use App\Repository\PostRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{
    public const POSTS_PER_PAGE = 8;

    /**
     * @Route("/", name="index_page")
     * @Template("index.html.twig")
     * @param PaginationFactory $factory
     * @param Request           $request
     * @param PostRepository    $postRepository
     *
     * @return array
     */
    public function indexAction(PaginationFactory $factory, Request $request, PostRepository $postRepository)
    {
        $section = $request->query->get('section');

        $qb = $postRepository->findAllQueryBuilder($section);

        $postCollection = $factory->createCollection($qb, $request, 'index_page', self::POSTS_PER_PAGE);

        return ['postCollection' => $postCollection];
    }

    /**
     * @Route("/about", name="about_page")
     * @Template("about.html.twig")
     */
    public function aboutAction()
    {

    }

    /**
     * @Route("/rules", name="rules_page")
     * @Template("rules.html.twig")
     */
    public function rulesAction()
    {

    }
}