<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 01/07/2018
 * Time: 14:41
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\UserFormType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="user_registration")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param UserRepository               $userRepository
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        UserRepository $userRepository
    ) {
        $user = new User();
        $form = $this->createForm(UserFormType::class, $user);

        // for a while only one user (admin) can register
        $existUser = $userRepository->find(1);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() && !$existUser) {

            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('index_page');
        }

        return $this->render('register.html.twig',
            ['registerForm' => $form->createView()]
        );
    }
}