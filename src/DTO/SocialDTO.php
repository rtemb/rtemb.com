<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 29/07/2018
 * Time: 14:02
 */

namespace App\DTO;

class SocialDTO
{
    /** @var string */
    private $title;

    /** @var string */
    private $description;

    /** @var string */
    private $image;

    /** @var string */
    private $url;

    /**
     * SocialDataDTO constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $propName => $val) {
            if (!property_exists($this, $propName)) {
                throw new \RuntimeException('Property ' . $propName . 'not exists in class ' . SocialDTO::class);
            }
            $this->$propName = $val;
        }
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}