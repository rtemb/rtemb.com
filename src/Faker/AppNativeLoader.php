<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 13/05/18
 * Time: 23:48
 */

namespace App\Faker;

use App\Faker\Provider\KeywordProvider;
use App\Faker\Provider\PreviewProvider;
use App\Faker\Provider\SectionProvider;
use Nelmio\Alice\Faker\Provider\AliceProvider;
use Nelmio\Alice\Loader\NativeLoader;
use Faker\Factory as FakerGeneratorFactory;
use Faker\Generator as FakerGenerator;

/**
 * Class AppNativeLoader
 *
 * @TODO    This loader need to split on several loaders for each fixture
 *
 * @package App\Faker
 */
class AppNativeLoader extends NativeLoader
{
    /** @var KeywordProvider */
    private $keywordProvider;

    /** @var PreviewProvider */
    private $previewProvider;

    /** @var SectionProvider */
    private $sectionProvider;

    /**
     * AppNativeLoader constructor.
     *
     * @param KeywordProvider $keywordProvider
     * @param PreviewProvider $previewProvider
     * @param SectionProvider $sectionProvider
     */
    public function __construct(
        KeywordProvider $keywordProvider,
        PreviewProvider $previewProvider,
        SectionProvider $sectionProvider
    ) {
        $this->keywordProvider = $keywordProvider;
        $this->previewProvider = $previewProvider;
        $this->sectionProvider = $sectionProvider;
        parent::__construct(null);
    }

    /**
     * @return FakerGenerator
     */
    protected function createFakerGenerator(): FakerGenerator
    {
        $generator = FakerGeneratorFactory::create(parent::LOCALE);
        $generator->addProvider(new AliceProvider());
        $generator->addProvider($this->keywordProvider);
        $generator->addProvider($this->previewProvider);
        $generator->addProvider($this->sectionProvider);
        $generator->seed($this->getSeed());

        return $generator;
    }
}