<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 13/05/18
 * Time: 22:29
 */

namespace App\Faker\Provider;

use Faker\Factory;

class PreviewProvider
{
    /**
     * @param int  $nbSentences
     * @param bool $variableNbSentences
     *
     * @param int  $width
     * @param int  $height
     *
     * @return string
     */
    public function previewProvider($nbSentences = 5, $variableNbSentences = true, $width = 420, $height = 220)
    {
        $faker = Factory::create();
        $text = $faker->paragraph($nbSentences, $variableNbSentences);

        $imgUrl = $faker->imageUrl($width, $height, null,true);
        $imgHtml = '<div align="center"> <img src="%s" width="%s" height="%s"/></div>';
        $image = sprintf($imgHtml, $imgUrl,'100%', '100%');

        return $image . '<p>' . $text . '</p>';
    }
}