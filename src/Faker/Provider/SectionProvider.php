<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 13/05/18
 * Time: 22:29
 */

namespace App\Faker\Provider;

use App\Entity\Section;
use App\Repository\SectionRepository;

class SectionProvider
{
    /** @var SectionRepository */
    private $sectionRepository;

    /** @var Section[] */
    private $sectionRegistry;

    public function __construct(SectionRepository $sectionRepository)
    {
        $this->sectionRepository = $sectionRepository;
    }

    /**
     * @throws \Exception
     */
    public function sectionProvider(): Section
    {
        if (!$this->sectionRegistry) {
            $this->sectionRegistry = $this->sectionRepository->findAll();
        }

        if (!$this->sectionRegistry) {
            throw new \Exception('No Sections found. Maybe SectionFixture should runs first.');
        }

        return $this->sectionRegistry[array_rand($this->sectionRegistry)];
    }
}