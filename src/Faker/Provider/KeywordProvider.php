<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 13/05/18
 * Time: 22:29
 */

namespace App\Faker\Provider;

class KeywordProvider
{
    private const KEYWORDS_SET = [
        'php,mysql,nginx,web',
        'docker,devops',
        'cat,Sniff',
        'game',
        'ardruino,DIY,hardware',
        'windows,PC'
    ];

    /**
     * @return string
     */
    public function keywordProvider()
    {
        return self::KEYWORDS_SET[array_rand(self::KEYWORDS_SET)];
    }
}