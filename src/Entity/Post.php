<?php

namespace App\Entity;

use App\DTO\SocialDTO;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="posts")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    public const POST_STATUS_PUBLISHED = 1;

    public const POST_STATUS_HIDDEN    = 2;

    public const POST_STATUS_IN_QUEUE  = 3;

    public static $statuses = [
        self::POST_STATUS_HIDDEN    => 'Hidden',
        self::POST_STATUS_IN_QUEUE  => 'In queue',
        self::POST_STATUS_PUBLISHED => 'Published',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=3000)
     */
    private $preview;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Section", inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $section;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateCreated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datePublished;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $keywords;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @var SocialDTO|null
     */
    private $socialData;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Post
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getPreview(): string
    {
        return $this->preview;
    }

    /**
     * @param string $preview
     *
     * @return Post
     */
    public function setPreview(string $preview): self
    {
        $this->preview = $preview;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return Post
     */
    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return Section|null
     */
    public function getSection(): ?Section
    {
        return $this->section;
    }

    /**
     * @param Section|null $section
     *
     * @return Post
     */
    public function setSection(?Section $section): self
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTimeInterface $dateCreated
     *
     * @return Post
     */
    public function setDateCreated(\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDatePublished(): ?\DateTimeInterface
    {
        return $this->datePublished;
    }

    /**
     * @param \DateTimeInterface|null $datePublished
     *
     * @return Post
     */
    public function setDatePublished(?\DateTimeInterface $datePublished): self
    {
        $this->datePublished = $datePublished;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    /**
     * @param null|string $keywords
     *
     * @return Post
     */
    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return Post
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return SocialDTO
     */
    public function getSocialData()
    {
        if (!$this->socialData) {
            $this->socialData = $this->initSocialData();
        }

        return $this->socialData;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersistUpdate()
    {
        $this->dateCreated = new \DateTime();
    }

    /**
     * @return SocialDTO
     */
    private function initSocialData(): SocialDTO
    {
        $socialData  = [];
        $description = [];
        preg_match_all('/(.*)>(.*)</', $this->getPreview(), $matches);
        if (array_key_exists(2, $matches) && array_key_exists(1, $matches[2])) {
            preg_match_all('/(.*)([^&nbsp;])/', $matches[2][1], $description);
        }
        preg_match_all('/(src=")(.*\.\w{3})/', $this->preview, $image);

        $socialData['title']       = $this->getTitle();
        $socialData['description'] = (array_key_exists(0, $description) && array_key_exists(0,
                $description[0])) ? $description[0][0] : '';

        $socialData['url']   = 'https://rtemb.com/post/read/' . $this->getId();
        $socialData['image'] = (array_key_exists(2, $image) && array_key_exists(0,
                $image[2])) ? $image[2][0] : '';

        return new SocialDTO($socialData);
    }
}
