<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 06/10/2018
 * Time: 22:28
 */

namespace App\DataFixtures\ORM\PostFixture;

use App\DataFixtures\ORM\SectionFixture\SectionFixture;
use App\Entity\Post;
use App\Faker\AppNativeLoader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixture extends Fixture implements ORMFixtureInterface, DependentFixtureInterface
{
    /** @var AppNativeLoader */
    private $appNativeLoader;

    public function __construct(AppNativeLoader $appNativeLoader)
    {
        $this->appNativeLoader = $appNativeLoader;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $objectSet = $this->appNativeLoader->loadFile(__DIR__ . '/post-fixture.yml');

        /** @var Post $post */
        foreach ($objectSet->getObjects() as $post) {
            $manager->persist($post);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            SectionFixture::class,
        ];
    }
}