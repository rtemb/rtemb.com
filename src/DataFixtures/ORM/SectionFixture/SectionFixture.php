<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 13/05/18
 * Time: 12:33
 */

namespace App\DataFixtures\ORM\SectionFixture;

use App\Entity\Section;
use App\Faker\AppNativeLoader;
use App\Repository\SectionRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SectionFixture extends Fixture implements ORMFixtureInterface
{
    /** @var SectionRepository */
    private $sectionRepository;

    /** @var AppNativeLoader */
    private $appNativeLoader;

    /**
     * SectionFixture constructor.
     *
     * @param AppNativeLoader   $appNativeLoader
     * @param SectionRepository $sectionRepository
     */
    public function __construct(AppNativeLoader $appNativeLoader, SectionRepository $sectionRepository)
    {
        $this->appNativeLoader   = $appNativeLoader;
        $this->sectionRepository = $sectionRepository;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $objectSet = $this->appNativeLoader->loadFile(__DIR__ . '/section-fixture.yml');

        /** @var Section $section */
        foreach ($objectSet->getObjects() as $section) {
            if (!$this->sectionRepository->findBy(['name' => $section->getName()])) {
                $manager->persist($section);
            }
        }

        $manager->flush();
    }
}