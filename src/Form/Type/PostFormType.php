<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 06/05/18
 * Time: 15:50
 */

namespace App\Form\Type;

use App\Entity\Post;
use App\Entity\Section;
use App\Repository\SectionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PostFormType
 *
 * @package App\Form\Type
 */
class PostFormType extends AbstractType
{
    /** @var SectionRepository */
    private $sectionRepository;

    /**
     * PostFormType constructor.
     *
     * @param SectionRepository $sectionRepository
     */
    public function __construct(SectionRepository $sectionRepository)
    {
        $this->sectionRepository = $sectionRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,
                [
                    'label'    => false,
                    'required' => true,
                ])
            ->add("preview", TextareaType::class,
                [
                    'label'    => false,
                    'required' => false,
                ])
            ->add('text', TextareaType::class,
                [
                    'label'    => false,
                    'required' => false,
                ])
            ->add('section', ChoiceType::class,
                [
                    'label'        => false,
                    'choices'      => $this->sectionRepository->findAll(),
                    'choice_label' => function ($section, $key, $index) {
                        /** @var Section $section */
                        return $section->getName();
                    },
                ])
            ->add('datePublished', DateTimeType::class,
                [
                    'label'      => false,
                    'required'   => true,
                    'empty_data' => new \DateTime(),
                ])
            ->add('status', ChoiceType::class,
                [
                    'label'        => false,
                    'choices'      => \array_keys(Post::$statuses),
                    'choice_label' => function ($value, $key, $index) {
                        return Post::$statuses[$value];
                    },
                ])
            ->add('keywords', TextType::class, [
                'label' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}