<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 13/05/18
 * Time: 19:24
 */

namespace App\Command;

use App\Entity\Section;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DatabaseInit
 *
 * @package App\Command
 */
class DatabaseInit extends Command
{

    /** @var EntityManagerInterface */
    private $em;

    /**
     * GeneratePosts constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('db:init')
            ->setDescription('Creates a new posts.')
            ->setHelp('This command allows you to create a some fake posts...');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sectionNames = ['Dev', 'Hardware', 'Others'];
        try {
            foreach ($sectionNames as $sectionName) {
                $exsistSection = $this->em->getRepository(Section::class)->findBy(['name' => $sectionName]);

                if (!$exsistSection) {
                    $section = (new Section())
                        ->setName($sectionName);

                    $this->em->persist($section);
                    $this->em->flush();
                    $output->writeln('Section "' . $sectionName . '" added');
                } else {
                    $output->writeln('Section "' . $sectionName . '" exist. Skipped');
                }
            }
            $output->writeln('All sections processed');
        } catch (Exception $e) {
            $output->writeln('Error: ' . $e->getMessage());
        }
    }
}