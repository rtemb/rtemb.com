self-written blog
==============

This is a source code of my blog https://www.rtemb.com running with Symfony 4 in Docker containers. 

# App installation
1. Ensure that you system has installed docker with docker-compose
```
>$ docker -v && docker-compose -v
```
2. Create .env file
```
>$ cp .env.dist .env
```
3. In .env file set APP_ENV variable (prod or dev). 
Set variables in MySQL section according with your database.
Uncomment TRUSTED_PROXIES, TRUSTED_HOSTS and xDebug variables

4. In your hosts file add entry with your host from TRUSTED_HOSTS variable:
```
127.0.0.1 rtemb.loc
```
5. For first installation run 
```
>$ make first-install-dev
```

# How it works?

Here are the `docker-compose` built images:

* `db`: This is the MySQL database container. All data mounts to local host to .data folder
* `php`: This is the PHP-FPM container including the application volume mounted on,
* `nginx`: This is the Nginx webserver container in which php volumes are mounted too.
* `yarn`: This is the container with Yarn and NodeJs. For DEV purposes only! Need to compile static files from assets to public folder.

This results in the following running containers:

```bash
> $ docker-compose ps
        Name                       Command               State              Ports
--------------------------------------------------------------------------------------------
dockersymfony_db_1      docker-entrypoint.sh mysqld      Up      0.0.0.0:3306->3306/tcp
dockersymfony_nginx_1   nginx                            Up      443/tcp, 0.0.0.0:80->80/tcp
dockersymfony_php_1     docker-php-entrypoint php-fpm    Up      0.0.0.0:9000->9000/tcp, 9010/tcp
rtembsymfony_yarn_1     node                             Up      3000/tcp 
```

# Frontend commands

```bash
# compile assets once
 ./node_modules/.bin/encore dev
# Or compile assets in container:
 docker-compose exec -T yarn yarn install
 docker-compose exec -T yarn ./node_modules/.bin/encore dev
```
```
# recompile assets automatically when files change
 ./node_modules/.bin/encore dev --watch
```
```
# compile assets, but also minify & optimize them
 ./node_modules/.bin/encore production
```
```
# shorter version of the above 3 commands"
 yarn run encore dev
 yarn run encore dev --watch
 yarn run encore production
```

# Database commands

```bash
# create database
php bin/console doctrine:database:create
```
```bash
# remove database
php bin/console doctrine:database:drop --force
```
```
# create database dump
# in DB container:
mysqldump -u user -p rtemb_db > rtemb_db-dump.sql
# On host:
docker cp db:/rtemb_db-dump.sql ./"rDB-dump-$(date '+%Y-%m-%d_%H:%M:%S').sql"
```
```
# import DB form dump
mysql -uroot -p rtemb_db < database.dump
```
```
# create new entity
php bin/console make:entity
```
```
# create DB migration file
php bin/console doctrine:migrations:diff
```
```
# run DB migration
php bin/console doctrine:migrations:migrate
```
```
# load DB fixtures
php bin/console doctrine:fixtures:load
```

# Ansible
```
# Deply new build on prod server
ansible-playbook -i ./hosts --ask-vault-pass --user=root deploy-playbook.yml 
```

# Useful Docker commands
```
# Copy file from docker container to host (docker cp 8e4f46b49fa3:/rtemb_db.sql . )
docker cp <containerId>:/file/path/within/container /host/path/target
```
```
# Copy file from host to docker container 
docker cp ./foo.txt mycontainer:/foo.txt
```

# Read logs
You can access Nginx and Symfony application logs in the following directories on your host machine:

* `containers/logs/nginx`
* `containers/logs/symfony`

# Tests
```
# Run unit tests
docker-compose exec php php  -dxdebug.remote_enable=1 -dxdebug.remote_mode=req -dxdebug.remote_port=9010 -dxdebug.remote_host=127.0.0.1 ./vendor/phpunit/phpunit/phpunit --configuration phpunit.xml.dist
```