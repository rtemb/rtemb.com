// Import TinyMCE
import tinymce from 'tinymce/tinymce';

// A theme is required
import 'tinymce/themes/modern/theme';

// Any plugins you want to use has to be imported
import 'tinymce/plugins/paste';
import 'tinymce/plugins/advlist';
import 'tinymce/plugins/autolink';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/image';
import 'tinymce/plugins/charmap';
import 'tinymce/plugins/autoresize';
import 'tinymce/plugins/codesample';
import 'tinymce/plugins/searchreplace';
import 'tinymce/plugins/visualblocks';
import 'tinymce/plugins/code';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/insertdatetime';
import 'tinymce/plugins/media';
import 'tinymce/plugins/table';
import 'tinymce/plugins/contextmenu';
import 'tinymce/plugins/imagetools';
import 'tinymce/plugins/anchor';
import 'tinymce/plugins/link';
import 'tinymce/plugins/print';
import 'tinymce/plugins/preview';

tinymce.init({
    selector: '#post_form_preview',
    theme: 'modern',
    height: 400,
    max_width: 600,
    min_height: 400,
    min_width: 600,
    setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    },
    plugins: [
        "advlist autolink lists link image charmap print preview anchor autoresize codesample",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste imagetools"
    ],
    toolbar: "insertfile undo redo | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | codesample",
});

tinymce.init({
    selector: '#post_form_text',
    theme: 'modern',
    height: 400,
    max_width: 600,
    min_height: 100,
    min_width: 600,
    setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    },
    plugins: [
        "advlist autolink lists link image charmap print preview anchor autoresize codesample",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste imagetools"
    ],
    toolbar: "insertfile undo redo | styleselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image codesample",
});