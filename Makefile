# Help
all:
	echo "run:> 'cat Makefile' to learn targets"

#=================================== GENERAL TARGETS ====================================
# Staring up all services
up:
	docker-compose up -d

# Shut down all services
down:
	docker-compose down

# Rebuild and run Docker containers
rebuild:
	docker-compose up --build -d

# Run post fake generation
fake-gen:
	docker-compose exec php php bin/console doctrine:fixture:load

# Run git pull command
pull:
	git pull
#=================================== /GENERAL TARGETS/ ==================================


#=================================== DEV TARGETS =====================================
# Full dev installation for first time
dev-first-install:
	docker-compose up --build -d && docker-compose exec php composer install && docker-compose exec yarn yarn install && docker-compose exec yarn ./node_modules/.bin/encore dev && docker-compose exec php php bin/console doctrine:migrations:migrate && docker-compose exec php php bin/console db:init && docker-compose exec php php bin/console doctrine:fixture:load
#=================================== /DEV TARGETS/ ===================================


#=================================== PROD TARGETS ====================================
# Full prod installation for first time
prod-first-install: server-install fetch-compiled-fe
	cp ./config/ci/.env-prod ./.env && docker-compose up --build -d && docker-compose exec -T php composer install --no-dev --optimize-autoloader --no-interaction && docker-compose exec -T php php bin/console doctrine:database:create --if-not-exists && docker-compose exec -T php php bin/console doctrine:migrations:migrate && docker-compose exec -T php php bin/console db:init && docker-compose exec -T nginx /le.sh

# Update app on prod server
prod-update: down pull fetch-compiled-fe
	docker-compose up --build -d && docker-compose exec -T php composer install --no-dev --optimize-autoloader --no-interaction && docker-compose exec -T php php bin/console doctrine:migrations:migrate

# Install necessary software on prod server
server-install:
	apt-get install docker && apt-get install docker-compose && apt-get install unzip

# Replace public folder on folder with compiled frontend files got from Gitlab
fetch-compiled-fe:
	rm -rf public && wget -O public.zip https://gitlab.com/rtemb/rtemb.com/-/jobs/artifacts/master/download?job=build && unzip public.zip && rm -rf public.zip
#=================================== /PROD TARGETS/ ==================================

