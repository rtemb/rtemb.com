<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 29/07/2018
 * Time: 21:52
 */

namespace App\Tests\Controller;

use App\Controller\MainController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MaintControllerTest extends WebTestCase
{
    use FooterChecker;

    /**
     * @group functional
     */
    public function testIndexAction()
    {
        $client = static::createClient();

        $crawler = $client->request(Request::METHOD_GET, '/');
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $this->assertEquals(3, count($crawler->filter('.nav-list')->getIterator()));

        $this->assertEquals(MainController::POSTS_PER_PAGE, count($crawler->filter('.post-preview-title')->getIterator()));
        $this->assertEquals(4, count($crawler->filter('.pagination-sm')->filter('li')->getIterator()));

        $this->checkFooter($response);
    }

    /**
     * @group functional
     */
    public function testAboutAction()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/about');
        $response = $client->getResponse();

        $this->assertContains('О сайте', $response->getContent());
        $this->assertContains('Об авторе', $response->getContent());

        $this->checkFooter($response);
    }

    /**
     * @group functional
     */
    public function testRulesAction()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/rules');
        $response = $client->getResponse();

        $this->assertContains('Правила', $response->getContent());

        $this->checkFooter($response);
    }
}