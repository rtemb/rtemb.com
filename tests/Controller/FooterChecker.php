<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 04/08/2018
 * Time: 20:03
 */

namespace App\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;

trait FooterChecker
{
    protected function checkFooter(Response $response)
    {
        $this->assertContains('Секции', $response->getContent());
        $this->assertContains('Dev', $response->getContent());
        $this->assertContains('Hardware', $response->getContent());
        $this->assertContains('Other', $response->getContent());

        $this->assertContains('Информация', $response->getContent());
        $this->assertContains('О сайте', $response->getContent());
        $this->assertContains('Правила', $response->getContent());
    }
}